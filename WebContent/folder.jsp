<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="folder" id="${tree.getFullNodeId()}_folder">

	<div class="folderName nonactive" id="${tree.getFullNodeId()}" style="margin-left:${offset}px" onclick="toggleActive('${tree.getFullNodeId()}')">
	
		<div class="folderImg" onclick="toggleActive('${tree.getFullNodeId()}')"></div>
		<input type="text" id="${tree.getFullNodeId()}_input" value="<c:out value="${tree.getData()}"/>" onBlur="rename()"/>
		
		<c:choose>
		
			<c:when test="${!tree.isChildsHidden()}">
				<div class="folderOpen" id="${tree.getFullNodeId()}_isOpen" onclick="toggleOpenClose('${tree.getFullNodeId()}')"></div>
			</c:when>
			
			<c:otherwise>
				<div class="folderClose" id="${tree.getFullNodeId()}_isOpen" onclick="toggleOpenClose('${tree.getFullNodeId()}')"></div>
			</c:otherwise>
			
		</c:choose>
		
		<img src="${pageContext.servletContext.contextPath}/images/trash.png" onclick="deleteNode('${tree.getFullNodeId()}')">
		<img src="${pageContext.servletContext.contextPath}/images/plus.png" onclick="addNode('${tree.getFullNodeId()}')">

	</div>
	
	<div class="folderChilds" id="${tree.getFullNodeId()}_childs">
		
		<c:if test="${!tree.isChildsHidden()}">
		
			<c:set var="offset" value="${offset + 20}" scope="request"/>
			
			<c:forEach var="child" items="${tree.getChilds()}">
			    <c:set var="tree" value="${child}" scope="request"/>
			    <jsp:include page="folder.jsp"/>
			</c:forEach>
			
			<c:set var="offset" value="${offset - 20}" scope="request"/>
			
		</c:if>
		
	</div>
	
</div>
