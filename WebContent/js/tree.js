function toggleActive(nodeId) {
	var actives = document.getElementsByClassName("active");
	var toActive = true;
	while(actives.length != 0) {
		if (actives[0] == event.target) toActive = false;
		let classList = actives[0].classList;
		classList.remove("active");
		classList.add("nonactive");
	}
	        
	if (toActive) {
		let classList = document.getElementById(nodeId).classList;
	    classList.remove("nonactive");
	    classList.add("active");
	}
}

function toggleOpenClose(nodeId) {
	if (event.target.classList.contains("folderClose")) {
		setOpen(nodeId);
	} else if (event.target.classList.contains("folderOpen")) {
		setClose(nodeId);
	}
	
}

function setOpen(nodeId) {
	document.getElementById(nodeId).classList.add("loading");
	$.post(
		"tree",
		{	method: "Open",
			nodeId: nodeId
		},
		function( data ) {
			document.getElementById(nodeId).classList.remove("loading");
			document.getElementById(nodeId + "_isOpen").classList.remove("folderClose");
			document.getElementById(nodeId + "_isOpen").classList.add("folderOpen");
			var childs = $(data).find("#" + nodeId + "_childs")[0];
			document.getElementById(nodeId + "_childs").replaceWith(childs);
		}
	);
}

function setClose(nodeId) {
	$.post(
			"tree",
			{	method: "Close",
				nodeId: nodeId
			},
			function( data ) {
				document.getElementById(nodeId + "_isOpen").classList.remove("folderOpen");
				document.getElementById(nodeId + "_isOpen").classList.add("folderClose");
				document.getElementById(nodeId + "_childs").innerHTML = "";
			}
		);
}

function addNode(parentId) {
	$.post(
		"tree",
		{	method: "Add",
			nodeId: parentId
		},
		function( data ) {
			var childs = $(data).find("#" + parentId + "_childs")[0];
			if (!document.getElementById(parentId + "_isOpen").classList.contains("folderOpen")) {
				setOpen(parentId);
			} else {
				document.getElementById(parentId + "_childs").replaceWith(childs);				
			}
		}
	);
}

function deleteNode(parentId) {
	$.post(
		"tree",
		{	method: "Delete",
			nodeId: parentId
		},
		function( data ) {
			document.getElementById(parentId + "_folder").remove();
		}
	);
}	


function rename() {
	var element = event.target;
	var newName = element.value;
	var id = element.parentNode.id;
	console.log(id);
	$.post(
			"tree",
			{
				method: "Rename",
				nodeId: id,
				name: newName
			}
		);
}