<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8" />
        <title>Tree view</title>
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/general.css" />
        <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/css/folder.css" />
        <script src="http://code.jquery.com/jquery-2.2.4.js" 
        		type="text/javascript"></script>
        <script src="js/app-ajax.js" type="text/javascript"></script>
    </head>
    <body>
    	<div id="wrap">
	    	<jsp:include page="/header.jsp"/>
	    	<div id="mainWindow">
		    	<div id="treeView">
		    		<jsp:include page="folder.jsp"/>
		        </div>
	        </div>
    	</div>
    	<script src="${pageContext.servletContext.contextPath}/js/tree.js"></script>
    </body>
</html>