package treeviewer;

import java.util.ArrayList;
import java.util.List;

public class TreeNode<T> {
	
	private TreeNode<T> parent;

	private int nodeId;
	private int nextChildId;
	
	private T data;
	
	private List<TreeNode<T>> childs;
	
	private boolean childsHidden;
	
	public TreeNode(T data) {
		this.data = data;
		childs = new ArrayList<>();
		nodeId = 0;
		nextChildId = 0;
		childsHidden = true;
	}
	
	public void setParent(TreeNode<T> parent) {
		this.parent = parent;
	}
	public TreeNode<T> getParent() {
		return parent;
	}
	
	public void setData(T data) {
		this.data = data;
	}
	public T getData() {
		return data;
	}

	public int getNodeId() {
		return nodeId;
	}
	public String getFullNodeId() {
		if (parent == null) return "" + nodeId;
		return parent.getFullNodeId() + "_" + nodeId;
	}
	
	public boolean isChildsHidden() {
		return childsHidden;
	}
	public void setChildsHidden(boolean childsHidden) {
		this.childsHidden = childsHidden;
	}
	
	public TreeNode<T> add(TreeNode<T> child) {
		child.setParent(this);
		child.nodeId = nextChildId++;
		childs.add(child);
		return child;
	}
	public TreeNode<T> add(T data) {
		TreeNode<T> child = new TreeNode<T>(data);
		return add(child);
	}
	public void add(List<TreeNode<T>> childs) {
		childs.forEach(child -> {
			child.setParent(this);
			child.nodeId = nextChildId++;
		});
		this.childs.addAll(childs);
	}
	
	public List<TreeNode<T>> getChilds() {
		return childs;
	}
}
