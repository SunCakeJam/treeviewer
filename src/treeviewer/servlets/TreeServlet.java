package treeviewer.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import treeviewer.TreeNode;

public class TreeServlet extends HttpServlet {
	
	private static final Logger LOGGER = Logger.getLogger(TreeServlet.class.getName());

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		TreeAction action;
		try {
			action = TreeAction.valueOf(req.getParameter("method"));		
		} catch (IllegalArgumentException | NullPointerException e) {
			LOGGER.log(Level.WARNING, "Wrong action");
			resp.setStatus(400);
			return;
		}
		
		LinkedList<Integer> nodePath = new LinkedList<>();
		String nodeId = req.getParameter("nodeId");
		if (nodeId == null) {
			resp.setStatus(400);
			LOGGER.log(Level.WARNING, "Don't get nodeId for action");
			return;
		}
		for(String id : nodeId.split("_")) {
			nodePath.add(Integer.valueOf(id));
		}

		TreeNode<String> node = getTree(req);
		if (node == null || node.getNodeId() != nodePath.get(0)) {
			resp.setStatus(400);
			LOGGER.log(Level.WARNING, "Tree not created");
			return;
		}
		for(int i = 1; i < nodePath.size(); i++)  {
			boolean found = false;
			for (TreeNode<String>  child : node.getChilds()) {
				if (child.getNodeId() == nodePath.get(i)) {
					found = true;
					node = child;
					break;
				} 
			}
			if (!found) {
				resp.setStatus(400);
				return;
			}
		}
		
		LOGGER.log(Level.INFO, "Try to do action:{0} with node:{1}", new String[] {action.toString(), nodeId});
		switch(action) {
			case Add:
				node.add("Node");
				node.setChildsHidden(false);
				req.getRequestDispatcher("/treeView.jsp").forward(req, resp);
				break;
			case Delete:
				TreeNode<String> parent = node.getParent();
				if (parent == null) {
					resp.setStatus(400);
					return;
				}
				parent.getChilds().remove(node);
				break;
			case Rename:
				node.setData(req.getParameter("name"));
				break;
			case Open:
				node.setChildsHidden(false);
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					LOGGER.log(Level.SEVERE, "Ooooops...");
				}
				req.getRequestDispatcher("/treeView.jsp").forward(req, resp);
				break;
			case Close:
				node.setChildsHidden(true);
				break;
			default:
				resp.setStatus(400);
				return;
		}
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException, IOException {
		
		TreeNode<String> tree = getTree(req);
		
		if (tree == null) {
			tree = new TreeNode<String>("Root");
			LOGGER.log(Level.INFO, "New tree created");
		}
		req.getServletContext().setAttribute("tree", tree);
		req.getServletContext().setAttribute("nodeId", tree.getNodeId());
		req.getServletContext().setAttribute("offset", 0);
		
		req.getServletContext().getRequestDispatcher("/treeView.jsp").forward(req, resp);
	}
	
	private TreeNode<String> getTree(HttpServletRequest req) {
		TreeNode<String> tree = null;
		Object treeObject = req.getServletContext().getAttribute("tree");
		if (treeObject instanceof TreeNode) {
			if (((TreeNode) treeObject).getData() instanceof String) {
				tree = (TreeNode<String>) treeObject;		
			}
		}
		return tree;
	}
	
}
