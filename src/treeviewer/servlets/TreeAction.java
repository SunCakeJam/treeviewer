package treeviewer.servlets;

public enum TreeAction {
	Add, Delete, Rename, Open, Close;
}
